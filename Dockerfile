FROM debian:11

RUN useradd -ms /bin/bash user

COPY dotfiles-backup /bin/dotfiles-backup
COPY tests/test.sh /bin/test-dotfiles-backup

USER user:user

RUN mkdir -p /home/user/.local/share/bash-completion/completions
RUN dotfiles-backup completions > /home/user/.local/share/bash-completion/completions/dotfiles-backup
RUN echo 'source /home/user/.local/share/bash-completion/completions/dotfiles-backup' >> /home/user/.bashrc

COPY --chown=user:user tests/backups /home/user/backups
COPY --chown=user:user tests/fs/ /home/user

WORKDIR /home/user
ENTRYPOINT ["/bin/test-dotfiles-backup"]
