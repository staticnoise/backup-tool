#!/usr/bin/env bash

warning() {
    echo -n "$(yellow WARNING:) "
    echo $@
}
