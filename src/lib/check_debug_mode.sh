#!/usr/bin/env bash
#
check_debug_mode() {
    if [[ -n "${args[--debug]}" ]]; then
        set -x
    fi
}
