#!/usr/bin/env bash

rsync_is_available() {
    which rsync 2>&1 1>/dev/null
}
