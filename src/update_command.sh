#!/usr/bin/env bash

check_debug_mode

readarray -t FILES < <(find "${args[BACKUP]}" -type f)

for f in "${FILES[@]}"; do 
    FILE_WITHOUT_PREFIX="${f#${args[BACKUP]}}"

    TARGET="${args[TARGET]}/${FILE_WITHOUT_PREFIX}"
    BACKUP="${args[BACKUP]}/${FILE_WITHOUT_PREFIX}"

    if [[ ! -f "${TARGET}" ]]; then
        warning "File $(basename ${BACKUP}) won't be updated"`
               `", because ${TARGET} does not exist."
        continue
    fi

    if [[ ! -r "${TARGET}" ]]; then
        warning "File $(basename ${BACKUP}) won't be updated"`
               `", because ${TARGET} is not readable."
        continue
    fi

    cp --preserve=mode "${TARGET}" "${BACKUP}"
done

