#!/usr/bin/env bash

check_debug_mode

if [[ $(find ${args[BACKUP]} -maxdepth 1 -wholename "${args[BACKUP]}/*" -not -name '.*' -print -quit) ]]; then
    cp --preserve=mode --recursive "${args[BACKUP]}"/* "${args[TARGET]}"
fi

if [[ $(find ${args[BACKUP]} -maxdepth 1 -wholename "${args[BACKUP]}/.*" -not -name '.' -not -name '..' -print -quit) ]]; then
    cp --preserve=mode --recursive "${args[BACKUP]}"/.[!.]* "${args[TARGET]}"
fi
