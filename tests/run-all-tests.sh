#!/usr/bin/env bash

podman run -it --rm localhost/backup-tool update
echo
podman run -it --rm localhost/backup-tool update-absolute-paths
echo
podman run -it --rm localhost/backup-tool restore
echo
podman run -it --rm localhost/backup-tool restore-absolute-paths
echo

