#!/usr/bin/env bash

err() {
    echo $@ >&2
}

check_file() {
    file="$1"
    backup="$2"

    cmp -s $file $backup

    if [[ $? -ne 0 ]]; then
        err "File contents do not match"
        err "File $file contains $(cat $file)"
        err "File $backup contains $(cat $backup)"
        exit 1
    fi

    perms_file=$(stat -c %A "$file")
    perms_backup=$(stat -c %A "$backup")
    
    if [[ $perms_file != $perms_backup ]]; then
        err "File permissions do not match"
        err "File $file is $perms_file"
        err "File $backup contains $perms_backup"
        exit 1
    fi
}

test-restore() {

    dotfiles-backup restore . backups

    files=$(find backups/ -type f -exec bash -c 'echo {} | cut -d"/" -f2-' \;)

    for file in $files; do
        check_file "$file" "backups/$file"
    done
}

test-restore-absolute-paths() {

    dotfiles-backup restore /home/user /home/user/backups

    files=$(find backups/ -type f -exec bash -c 'echo {} | cut -d"/" -f2-' \;)

    for file in $files; do
        check_file "$file" "backups/$file"
    done
}

test-update() {

    dotfiles-backup update . backups

    files=$(find backups/ -type f -not -wholename '*backup-only*' -exec bash -c 'echo {} | cut -d"/" -f2-' \;)

    for file in $files; do
        check_file "$file" "backups/$file"
    done
}

test-update-absolute-paths() {

    dotfiles-backup update /home/user/ /home/user/backups/

    files=$(find backups/ -type f -not -wholename '*backup-only*' -exec bash -c 'echo {} | cut -d"/" -f2-' \;)

    for file in $files; do
        check_file "$file" "backups/$file"
    done
}

case "$1" in

    restore)
        test-restore;
        echo finished $1
   ;;

    restore-absolute-paths)
        test-restore-absolute-paths
        echo finished $1
    ;;

    update)
        test-update
        echo finished $1
    ;;

    update-absolute-paths)
        test-update-absolute-paths
        echo finished $1
    ;;

    *)
        err Usage: $0 '<restore|restore-absolute-paths|update|update-absolute-paths>'
        exit 1
    ;;
esac
